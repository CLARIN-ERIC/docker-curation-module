#!/usr/bin/bash

DATABASE_HOST=$(echo $DATABASE_URI | sed -E 's/jdbc:[a-z]+:\/\/(.*):([0-9]+)\/([a-z]+).*/\1/')
DATABASE_PORT=$(echo $DATABASE_URI | sed -E 's/jdbc:[a-z]+:\/\/(.*):([0-9]+)\/([a-z]+).*/\2/')
DATABASE_NAME=$(echo $DATABASE_URI | sed -E 's/jdbc:[a-z]+:\/\/(.*):([0-9]+)\/([a-z]+).*/\3/')

wait-for "${DATABASE_HOST}:${DATABASE_PORT}" && \
  mysql --host="${DATABASE_HOST}" --port="${DATABASE_PORT}" --user="${DATABASE_USERNAME}" --password="${DATABASE_PASSWORD}" "${DATABASE_NAME}" \
  -s -e "select 1" > /dev/null
