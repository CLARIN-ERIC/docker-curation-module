ARG CURATION_BUILD_DIRECTORY=/build

##########################################################################################
########## Build image (maven build of curation module) ##################################
##########################################################################################
FROM registry.gitlab.com/clarin-eric/docker-alpine-supervisor-java-base:openjdk11-2.1.9  AS curation-build

ARG TEMP=/tmp/repo/
ARG CURATION_BUILD_DIRECTORY
ARG CURATION_GIT_DIRECTORY=/curation-git

RUN apk --no-cache add curl bash maven	

COPY maven_repository /root/.m2/repository

#Now copy everything and build project

COPY curation-src $CURATION_GIT_DIRECTORY

RUN ls $CURATION_GIT_DIRECTORY

RUN cd "$CURATION_GIT_DIRECTORY" \
 && mvn install -DskipTests \
 && mkdir -p "$CURATION_BUILD_DIRECTORY" "$CURATION_BUILD_DIRECTORY"/bin "$CURATION_BUILD_DIRECTORY"/conf "$CURATION_BUILD_DIRECTORY"/lib \
 && cp "$CURATION_GIT_DIRECTORY"/curation-core/target/curation-core-*.jar $CURATION_BUILD_DIRECTORY/bin/curation.jar \
 && cp "$CURATION_GIT_DIRECTORY"/curation-core/target/lib/* $CURATION_BUILD_DIRECTORY/lib/ \
 && cp "$CURATION_GIT_DIRECTORY"/curation-core/script/updateDashboard.sh $CURATION_BUILD_DIRECTORY/bin/updateDashboard.sh \
 && cp "$CURATION_GIT_DIRECTORY"/curation-web/target/curation.war $CURATION_BUILD_DIRECTORY/bin/curation.war \
 && cp -r "$CURATION_GIT_DIRECTORY"/curation-core/src/main/resources/conf/* "$CURATION_BUILD_DIRECTORY"/conf/ \
 && cp "$CURATION_GIT_DIRECTORY"/curation-core/src/main/resources/template_config.properties "$CURATION_BUILD_DIRECTORY"/conf/config.properties \
 && rm -rf "$CURATION_GIT_DIRECTORY"


##########################################################################################

##########################################################################################
########### Final image ##################################################################
##########################################################################################
FROM registry.gitlab.com/clarin-eric/docker-alpine-supervisor-java-tomcat-base:8.5.78-java11_1.0.2

ARG CURATION_BUILD_DIRECTORY

RUN apk add mysql-client

ENV CURATION_DIRECTORY=/usr/local/curation-module
ENV CONFIG_LOCATION=${CURATION_DIRECTORY}/conf/config.properties

COPY --from=curation-build $CURATION_BUILD_DIRECTORY $CURATION_DIRECTORY

WORKDIR $CURATION_DIRECTORY
RUN mkdir -p bin conf conf data logs public value_maps xsd_cache xsd_cache/private_profiles \
 && chmod 777 xsd_cache xsd_cache/private_profiles
WORKDIR $CURATION_DIRECTORY/public
RUN mkdir -p html/collections html/instances html/profiles html/statistics xml/collections xml/instances xml/profiles xml/statistics \
 && chmod 777 html/instances xml/instances xml/profiles
WORKDIR $CURATION_DIRECTORY

RUN rm -rf $CATALINA_HOME/webapps/ROOT.war $CATALINA_HOME/webapps/ROOT 
RUN mv $CURATION_DIRECTORY/bin/curation.war $CATALINA_HOME/webapps/ROOT.war


#log aggregation
COPY fluentd/updateDashboard.conf /etc/fluentd/conf.d/updateDashboard.conf
COPY fluentd/curation-module-web.conf /etc/fluentd/conf.d/curation-module-web.conf

#log rotation
COPY logrotate/updateDashboard.conf /etc/logrotate.d/updateDashboard.conf
RUN chmod 600 /etc/logrotate.conf /etc/logrotate.d/*

COPY init.sh /init/init.sh
COPY waitForDB.sh /init/waitForDB.sh
##########################################################################################
